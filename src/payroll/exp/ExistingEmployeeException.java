/**
 * @file ExistingEmployeeException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement ExistingEmployeeException exception
 */
package payroll.exp;

public class ExistingEmployeeException extends PayrollUncheckedException {
	public ExistingEmployeeException() { }
	public ExistingEmployeeException(String m) {
		super(m);
	}
}
