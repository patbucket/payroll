/**
 * @file InvalidLevelException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement InvalidLevelException exception
 */
package payroll.exp;

public class InvalidLevelException extends PayrollUncheckedException {
	public InvalidLevelException() { }

	public InvalidLevelException(String m) {
		super(m);
	}
}
