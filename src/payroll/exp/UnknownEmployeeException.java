/**
 * @file UnknownEmployeeException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement UnknownEmployeeException exception
 */
package payroll.exp;

public class UnknownEmployeeException extends PayrollUncheckedException {
	public UnknownEmployeeException() { }
	public UnknownEmployeeException(String m) {
		super(m);
	}
}
