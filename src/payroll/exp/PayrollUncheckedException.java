/**
 * @file PayrollUncheckedException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement PayrollUncheckedException exception
 */
package payroll.exp;

public class PayrollUncheckedException extends RuntimeException {
	public PayrollUncheckedException() { }
	public PayrollUncheckedException(String m)	{
		super(m);
	}
	
}
