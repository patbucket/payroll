/**
 * @file PayrollCheckedException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement PayrollCheckedException exception
 */
package payroll.exp;

public class PayrollCheckedException extends Exception {
	public PayrollCheckedException() { }
	public PayrollCheckedException(String m)	{
		super(m);
	}
}
