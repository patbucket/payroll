/**
 * @file InvalidOperationException.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement InvalidOperationException exception
 */
package payroll.exp;

public class InvalidOperationException extends PayrollCheckedException {
	public InvalidOperationException() {}
	public InvalidOperationException(String m)	{
		super(m);
	}
}
