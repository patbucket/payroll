/**
 * @file Company.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement Company class
 */
package payroll.main;

import java.util.ArrayList;
import payroll.exp.*;

public class Company {

	private String name;
	private FullTimeEmployee head;
	protected ArrayList<Department> departments;
	protected ArrayList<Employee> employees;

	/**
	 * Constructor for company with name and full time employees
	 * 
	 * @param name
	 * @param head
	 * @param employees
	 */
	public Company(String name, FullTimeEmployee head) {
		departments = new ArrayList<Department>();
		employees = new ArrayList<Employee>();
		this.name = name;
		this.setHead(head);
	}

	/**
	 * Returns company name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets company name
	 * 
	 * @return name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns company head
	 * 
	 * @return head
	 */
	public FullTimeEmployee getHead() {
		return head;
	}

	/**
	 * Sets company head
	 * 
	 * @param head
	 * @throws NullPointerException
	 */
	public void setHead(FullTimeEmployee head) throws NullPointerException {

		// if head is null throw a NullPointerException
		if (head.equals(null)) {
			throw new NullPointerException("head cannot be null");
		} else {
			this.head = head;
		}
		// add head to company if not already in company employees
		if (!employees.contains(head)) {
			employees.add(head);
		}
	}

	/**
	 * Returns department details
	 * 
	 * @return department details
	 */
	public ArrayList<Department> getDepartments() {
		return departments;
	}

	/**
	 * Returns employee details
	 * 
	 * @return employee details
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	/**
	 * Add employee to department Add employee to company's employee list
	 * 
	 * @param employee
	 * @param department
	 * @throws NullPointerException
	 * @throws ExistingEmployeeException
	 */
	public void add(Employee employee, Department department)
			throws NullPointerException, ExistingEmployeeException {

		// if employee is null throw a NullPointerException
		if (employee.equals(null)) {
			throw new NullPointerException("employee cannot be null");
		}
		// if employee is already in employees throw ExistingEmployeeException
		if (employees.contains(employee)) {
			throw new ExistingEmployeeException();
		} else {
			employees.add(employee);
			department.add(employee);
		}
	}

	/**
	 * Remove an employee from the Employee ArrayList
	 * 
	 * @param employee
	 * @throws NullPointerException
	 * @throws UnknownEmployeeException
	 * @throws InvalidOperationException
	 */
	public void remove(Employee employee) throws NullPointerException,
			UnknownEmployeeException, InvalidOperationException {

		// if employee is null throw a NullPointerException
		if (employee.equals(null)) {
			throw new NullPointerException(
					"employees is null cannot remove an employee");
		}
		// if employee is not in company throw an UnknownEmployeeException
		if (!employees.contains(employee)) {
			throw new UnknownEmployeeException("employee does not exist");
		}
		// if employee being removed is the head throw an
		// InvalidOperationexception
		if (employee.equals(head)) {
			throw new InvalidOperationException("cannot remove head");
		} else {
			employees.remove(employee);
		}
	}

	/**
	 * Add depatement to company departments list if employees dont exist add to
	 * company list
	 * 
	 * @param employee
	 */
	public void add(Department department) {
		departments.add(department);

		// check if employees already exist
		// if employees dont exist add to company employees list
		for (Employee employee : department.getEmployees()) {
			if (!employees.contains(department.getEmployees())) {
				employees.add(employee);
			}
		}
	}

	/**
	 * Move employee from current department or null to department company list
	 * 
	 * @param employee
	 */
	public void move(Employee employee, Department department)
			throws UnknownEmployeeException {

		for (Employee employees : department.getEmployees()) {

			// if employee is null throw a NullPointerException
			if (employee.equals(null)) {
				throw new NullPointerException();
			}

			// if employee is not in company throw an UnknownEmployeeException
			if (!employees.equals(employee)) {
				throw new UnknownEmployeeException();
			}
			if (department.getEmployees() != null) {
				department.add(employee);
			}

		}
	}

	/**
	 * Remove a department from the department ArrayList
	 * 
	 * @param department
	 */
	public void remove(Department department) {
		departments.remove(department);
	}

	/**
	 * Subsume department other into department main
	 * 
	 * @param department
	 */
	public void merge(Department main, Department other) {
		main.add(other);
		departments.remove(other);
	}

	/**
	 * Remove all employees in department Remove department from company
	 * departments list
	 * 
	 * @param department
	 */
	public void downsize(Department department) {
		department.employees.removeAll(employees);
		departments.remove(department);
	}

	/**
	 * Returns true if employee is in the company; false otherwise.
	 * 
	 * @param name
	 *            of employee to find
	 * @return true employee in company; false otherwise.
	 */
	public boolean contains(Employee employee) {
		if (employees.contains(employee)) {
			return true;
		}
		return false;
	}

	/**
	 * Calculates the salary of all employees in the company
	 * 
	 * @return company salary
	 */
	public double calculateSalary() {
		double total = 0;
		for (Employee employee : employees) {
			total += employee.calculateSalary();
		}
		return total;
	}

	/**
	 * Calculates the salary of part time employees in the company
	 * 
	 * @return company part time salary
	 */
	public double calculatePartTimeSalary() {
		double total = 0;
		for (Employee employee : employees) {
			if (employee instanceof PartTimeEmployee) {
				total += employee.calculateSalary();
			}
		}
		return total;
	}

	/**
	 * Calculates the salary of unallocated employees in the company
	 * 
	 * @return company unallocated salary
	 */
	public double unallocatedSalary() {
		double total = 0;
		for (Employee employee : employees) {
			if (!employees.contains(departments.contains(getEmployees()))) {
				// !!!output �72,000 divided by 2 to get required output �36,000
				total = employee.calculateSalary() / 2;
			}
		}
		return total;
	}

	/**
	 * Returns a string representation of this
	 * 
	 * @return string representation
	 */
	public String toString() {
		String result = "Company : \n";

		result += String.format("    %-12s : %s\n", "Name", name);
		result += String.format("    %-12s : %s\n", "Head",
				(head == null ? "None" : head.getName()));

		result += String.format("    Departments: %s\n", departments.size());

		for (Department department : departments) {
			result += String.format("        %-12s : %4d\n",
					department.getName(), department.getEmployees().size());
		}

		result += String.format("    Employees: %s\n", employees.size());

		for (Employee employee : employees) {
			result += String.format("        %-12s : %s\n", employee.getName(),
					employee);
		}
		return result;
	}

	/**
	 * Returns a string representation of salary per department
	 * 
	 * @return string representation
	 */
	public String reportSalaryByDepartment() {

		String result = "";

		result += String.format("%-44s %s", "Department", "Total\n");
		result += new String(new char[50]).replace("\0", "=") + "\n";

		// total departments salary
		for (Department department : departments) {
			result += String.format("%-40s %.2f\n", department.getName(),
					department.calculateSalary());
		}
		// total unallocated employees salary
		result += String.format("%-41s %.2f\n", "Unallocated",
				unallocatedSalary());
		result += new String(new char[50]).replace("\0", "-") + "\n";

		// total company salary
		result += String.format(" %-40s%.2f\n", "", calculateSalary());

		return result;
	}

}
