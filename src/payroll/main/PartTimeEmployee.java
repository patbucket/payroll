/**
 * @file PartTimeEmployee.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement PartTimeEmployee class
 */
package payroll.main;

public class PartTimeEmployee extends Employee {
	private int hours;
	private double rate;

	/**
	 * Constructor for PartTimeEmployee with id, name, hours, and rate
	 * 
	 * @param id
	 * @param name
	 * @param hours
	 * @param rate
	 */
	public PartTimeEmployee(int id, String name, int hours, double rate) {
		super(id, name);
		setHours(hours);
		setRate(rate);
	}

	/**
	 * Returns PartTimeEmployee hours
	 * 
	 * @return hours
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * Sets PartTimeEmployee hours
	 * 
	 * @param hours
	 */
	public void setHours(int hours) {
		this.hours = hours;
	}

	/**
	 * Returns PartTimeEmployee rate
	 * 
	 * @return rate
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * Sets PartTimeEmployee rate
	 * 
	 * @param rate
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}

	// generate class name
	String className = this.getClass().getSimpleName();

	/**
	 * Returns a string representation of this
	 * 
	 * @return string representation
	 */
	public String toString() {
		return String.format("%-20s %s, hours=%s, rate=%s", className,
				super.toString(), hours, rate);
	}

	/**
	 * Calculates the salary of PartTimeEmployee
	 * 
	 * @return PartTimeEmployee salary
	 */
	@Override
	public double calculateSalary() {
		return 12 * rate * hours;
	}
	
	//added to pass JUnit test
	public Object getID() {
		return getId();
	}

}
