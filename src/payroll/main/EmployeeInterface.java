/**
 * @file EmployeeInterface.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement EmployeeInterface class
 */
package payroll.main;

public interface EmployeeInterface {
	
	public int getId();
	public String getName();
	public double calculateSalary();
	public String toString();

}
