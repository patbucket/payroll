/**
 * @file FullTimeEmployee.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement FullTimeEmployee class
 */
package payroll.main;

import payroll.exp.InvalidLevelException;

public class FullTimeEmployee extends Employee {
	
	//salary lookup table
	private static final int[] SALARY_LEVELS = { 3000, 4000, 5000, 5500, 6000 };
	private int level;

	/**
	 * Constructor for FullTimeEmployee with id, name and level
	 * 
	 * @param id
	 * @param name
	 * @param level
	 */
	public FullTimeEmployee(int id, String name, int level) {
		super(id, name);
		setLevel(level);
	}

	/**
	 * Returns FullTimeEmployee level
	 * 
	 * @return level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Sets FullTimeEmployee level
	 * 
	 * @param level
	 */
	public void setLevel(int level) throws InvalidLevelException{
		if(level >=1 && level<=5)	{
			this.level =level;
		}else{
			throw new InvalidLevelException();
		}
	}
	
	// Generates Class name
	String className = this.getClass().getSimpleName();

	/**
	 * Returns a string representation of this
	 * 
	 * @return string representation
	 */
	public String toString() {
		return String.format("%-20s %s, level=%s", 
				className, super.toString(),getLevel());
	}

	/**
	 * Calculates the salary of FullTimeEmployee
	 * 
	 * @return FullTimeEmployee salary
	 */
	@Override
	public double calculateSalary() {
		return 12 * SALARY_LEVELS[getLevel() - 1];
	}

	

	//added to pass JUnit test
	public Object getID() {
		return getId();
	}

	

}
