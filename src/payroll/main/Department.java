/**
 * @file Department.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement Department class
 */
package payroll.main;

import java.util.ArrayList;
import payroll.exp.*;

public class Department {

	private String name;
	private FullTimeEmployee head;
	protected ArrayList<Employee> employees;

	/**
	 * Constructor for department with name, head, and employees
	 * 
	 * @param name
	 * @param head
	 * @param employees
	 */
	public Department(String name, FullTimeEmployee head) {
		this.employees = new ArrayList<Employee>();
		this.name = name;
		this.setHead(head);
	}

	/**
	 * Returns department name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets department name
	 * 
	 * @return name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns department head
	 * 
	 * @return head
	 */
	public FullTimeEmployee getHead() {
		return head;
	}

	/**
	 * Sets department head
	 * 
	 * @param head
	 * @throws NullPointerException
	 */
	public void setHead(FullTimeEmployee head) throws NullPointerException {

		// Throw a NullPointerException if head is null else set head
		if (head.equals(null)) {
			throw new NullPointerException("head cannot be null");
		} else {
			this.head = head;
		}
		// add head to department if not already in department employees
		if (!employees.contains(head)) {
			employees.add(head);
		}
	}

	/**
	 * Returns employee details
	 * 
	 * @return employee details
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	/**
	 * Adds an employee to the Employee ArrayList
	 * 
	 * @param employee
	 * @throws NullPointerException
	 * @throws ExistingEmployeeException
	 */
	public void add(Employee employee) throws NullPointerException,
			ExistingEmployeeException {

		// if employee is null throw a NullPointerException
		if (employee.equals(null)) {
			throw new NullPointerException("employee cannot be null");
		}

		// if employee already exists in employees throw a
		// ExistingEmployeeException
		if (employees.contains(employee)) {
			throw new ExistingEmployeeException(employee.toString());
		} else {
			employees.add(employee);
		}

	}

	/**
	 * Subsumes other department into this department
	 * 
	 * @param department
	 */
	public void add(Department other) {
		for (Employee otrEmployee : other.getEmployees()) {
			employees.addAll(other.getEmployees());
			other.employees.remove(otrEmployee);
		}
	}

	/**
	 * Remove an employee from the Employee ArrayList
	 * 
	 * @param employee
	 * @throws NullPointerException
	 * @throws UnknownEmployeeException
	 * @throws InvalidOperationException
	 */
	public void remove(Employee employee) throws NullPointerException,
			UnknownEmployeeException, InvalidOperationException {

		// if employee is null throw a NullPointerException
		if (employee.equals(null)) {
			throw new NullPointerException("no employee to remove");
		}

		// if employee is not in department throw an UnknownEmployeeException
		if (!employees.contains(employee)) {
			throw new UnknownEmployeeException(employee.toString());
		}

		// if employee been removed is the head of department throw
		// InvalidOperationException
		if (employee.equals(head)) {
			throw new InvalidOperationException("cannot remove head");
		} else {
			employees.remove(employee);
		}
	}

	/**
	 * Returns true if employee is in the department; false otherwise.
	 * 
	 * @param name
	 *            of employee to find
	 * @return true employee in department; false otherwise.
	 */
	public boolean contains(Employee employee) {
		if (employees.contains(employee)) {
			return true;
		}
		return false;
	}

	/**
	 * Calculates the salary of all employees in the department
	 * 
	 * @return department salary
	 */
	public double calculateSalary() {
		double total = 0;
		for (Employee employee : employees) {
			total += employee.calculateSalary();
		}
		return total;
	}

	/**
	 * Calculates the salary of part time employees in the department
	 * 
	 * @return department part time salary
	 */
	public double calculatePartTimeSalary() {
		double total = 0;
		for (Employee employee : employees) {
			if (employee instanceof PartTimeEmployee) {
				total += employee.calculateSalary();
			}
		}
		return total;
	}

	/**
	 * Returns a string representation of this
	 * 
	 * @return string representation
	 */
	public String toString() {
		String result = "Department : \n";

		result += String.format("    %-12s : %s\n", "Name", name);
		result += String.format("    %-12s : %s\n", "Head",
				(head == null ? null : head.getName()));
		result += String.format("    Staff:\n");
		for (Employee employee : employees) {
			result += "        " + employee + "\n";
		}
		return result;
	}
}
