/**
 * @file Employee.java
 * @author Patrick Murphy 99162576
 * @practical lab-12
 * @brief implement Employee class
 */
package payroll.main;

public abstract class Employee {
	private int id;
	private String name;
	
	
	/**
	 * Constructor for Employee with id and name
	 * 
	 * @param id
	 * @param name
	 */
	public Employee(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	/**
	 * Returns employee id
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns employee name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	
	abstract public double calculateSalary();
	
	/**
	 * Returns a string representation of this
	 * 
	 * @return string representation
	 */
	public String toString()	{
		return String.format("id=" 
				+ id + ", name=" + name  );
	}

}
